# GitLab CI Pipeline for NPM Audit

## Usage

### GitLab CI Include

With GitLab CI you can include a pipeline file.
Make sure you have added the `pre_build` stage to your pipeline.

```yaml
variables:
  IMAGE_COMMIT: $CI_REGISTRY_IMAGE/commit:$CI_COMMIT_SHA

include:
  - project: jeff_cook/npm-audit
    ref: master
    file: /pipeline.yml

stages:
  - test

NPM Audit:
  stage: test
```

#### Pipeline Configuration

- `NPM_AUDIT_ENABLED` set to false to disable the `NPM Audit` job.
- `IMAGE_COMMIT` variable will be used to run `npm audit`.
  Ensure this is set correctly for your project.
